let collection = [];

// Write the queue functions below.

function print() {
  return(collection);
}

function enqueue(item) {
  collection.push(item);
  return collection
}

function dequeue(item) {
  return collection.shift();
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};